import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Constant } from '../../Constant';


export class AddCarsScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      matricule: '',
      carburant_type: '',
      cheveaux: '',
      adresse: '',
      prix: '',
      infoVoiture: [],
    }
    this.onCreateVoiture = this.onCreateVoiture.bind(this)
  }

  onCreateVoiture = ({infoVoiture}) => {
    const { matricule, carburan_type, cheveaux, adresse, prix } = this.state;

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "matricule": matricule,
      "carburan_type": carburan_type,
      "cheveaux": cheveaux,
      "adresse": adresse,
      "prix": prix
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch(Constant.urlVoitureNew, requestOptions)
      .then(response => response.json())
      .then(result => console.log('FETCH-Create-Voiture:' + result))
      .catch(error => console.log('error', error));

    this.props.navigation.navigate('Profile');
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Matricule"
            keyboardType="name"
            underlineColorAndroid='transparent'
            onChangeText={(matricule) => this.setState({ matricule })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='camera' color='#124369' size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="type de carburant"
            keyboardType="name"
            underlineColorAndroid='transparent'
            onChangeText={(carburan_type) => this.setState({ carburan_type })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='text' color="#124369" size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="cheveaux"
            keyboardType="name"
            underlineColorAndroid='transparent'
            onChangeText={(cheveaux) => this.setState({ cheveaux })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='text' color="#124369" size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="adresse du vehicule"
            keyboardType="name"
            underlineColorAndroid='transparent'
            onChangeText={(adresse) => this.setState({ adresse })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='text' color="#124369" size={26} />
        </View>
        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
            placeholder="Prix / mois"
            keyboardType="name"
            underlineColorAndroid='transparent'
            onChangeText={(prix) => this.setState({ prix })} />
          <MaterialCommunityIcons style={styles.inputIcon} name='text' color="#124369" size={26} />
        </View>
        <TouchableOpacity style={styles.buttonContainer} onPress={() => this.onCreateVoiture('Create Voiture')}>
          <Text style={styles.btnText}>suivant</Text>
        </TouchableOpacity>
      </View>
    );
  };
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderRadius: 30,
    borderBottomWidth: 1,
    width: 300,
    height: 45,
    marginBottom: 20,
    flexDirection: 'row',
    alignItems: 'center',

    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  inputs: {
    height: 45,
    marginLeft: 16,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  inputIcon: {
    width: 30,
    height: 30,
    marginRight: 15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
    borderRadius: 30,
    backgroundColor: 'transparent'
  },
  btnText: {
    color: "white",
    fontWeight: 'bold'
  }
});