import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import ProfilScreen from './screen/ProfilScreen';
import ListDispo from './Recap/ListDispoScreen';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUser, fetchUserPosts } from '../redux/actions/index';

const Tab = createMaterialBottomTabNavigator();

const EmptyScreen = () => {
    return (null)
}
 class Main extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        this.props.fetchUser();
        this.props.fetchUserPosts();
    }
    render() {
        const {currentUser, posts} = this.props;
        return (
            <Tab.Navigator 
            initialRouteName='Profile' 
            labeled={false}
            barStyle={{ backgroundColor: '#124369' }}
            >
                <Tab.Screen name="Profile" component={ProfilScreen} 
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name='account-circle' color={color} size={26} />
                        )
                    }} />
                <Tab.Screen name="List Disponible" component={ListDispo}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name='magnify' color={color} size={26} />
                        )
                    }} />
                <Tab.Screen name="CarContainer" component={EmptyScreen}
                    listeners={({ navigation }) => ({
                        tabPress: event => {
                            event.preventDefault();
                            navigation.navigate("Voiture")
                        }
                    })}
                    options={{
                        tabBarIcon: ({ color, size }) => (
                            <MaterialCommunityIcons name='pencil' color={color} size={26} />
                        )
                    }} />
            </Tab.Navigator>
        )
    }
}

const mapStateToProps = (store) => ({
    currentUser: store.userState.currentUser,
    posts: store.userState.posts
})
const mapDispatchProps = (dispatch) => bindActionCreators({ fetchUser, fetchUserPosts }, dispatch);
export default connect(mapStateToProps, mapDispatchProps)(Main);