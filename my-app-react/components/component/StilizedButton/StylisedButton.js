import React, { FC } from 'react';
import { Text } from 'react-native';
import { Button } from 'react-native-paper';
import { MaterialIcons } from '@expo/vector-icons';
import { getStyles } from './StylizedButton.style';
import { StylizedButtonComponent } from './StylizedButton.type';

/**
 * StylizedButton with several setting available.
 */
export const StylizedButton: FC<StylizedButtonComponent> = ({
  children,
  onPress,
  leadingIcon = '',
  leadingIconColor,
  modeButton = 'text',
  uppercase = true,
  sizeIcon = 24,
  CatIcon = MaterialIcons,
  style,
  styleLabel,
  disable = false
}) => {
  const styles = getStyles(modeButton, disable);
  return (
    <>
      {leadingIcon !== '' ? (
        <Button
          mode={modeButton}
          onPress={onPress}
          style={[styles.box, style]}
          uppercase={uppercase}
          labelStyle={[styles.label, styleLabel]}
          disabled={disable}
          icon={() => (
            <CatIcon
              name={leadingIcon}
              size={sizeIcon}
              color={leadingIconColor}
              style={styles.leadingIcon}
            />
          )}
        >
          <Text style={styles.label}>{children}</Text>
        </Button>
      ) : (
        <Button
          mode={modeButton}
          onPress={onPress}
          style={[styles.box, style]}
          uppercase={uppercase}
          labelStyle={[styles.label, styleLabel]}
          disabled={disable}
        >
          <Text style={styles.label}>{children}</Text>
        </Button>
      )}
    </>
  );
};